 const API = {
  LOGIN: "login",
  REGISTER: "register",
  BOOK: "books",
  IS_LOGIN: "isLogin",
};

export default API