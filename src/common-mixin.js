const commonMixin = {
  data: function() {
    return {};
  },
  methods: {
    showModal(modalId) {
      this.$root.$emit("bv::show::modal", modalId);
    },
    hideModal(modalId) {
      this.$root.$emit("bv::hide::modal", modalId);
    },
  },
};
export default commonMixin;
