import Axios from "axios";
import _ from "underscore";

const axiosMixin = {
  data: function() {
    return {};
  },
  methods: {
    getQueryStringFromObject(obj) {
      const str = [];
      for (const p in obj) {
        if (_.has(obj, p)) {
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
      }
      return str.join("&");
    },

    postApi(endPoint, data, queryParam = {}, isTokenRequired = true) {
      let url;
      let options = {
        headers: {
          "Content-Type": "application/json",
        },
      };
      if (!_.isEmpty(queryParam)) {
        const queryParamString = this.getQueryStringFromObject(queryParam);
        url = `${endPoint}?${queryParamString}`;
      } else {
        url = endPoint;
      }
      data = data ? data : {};
      if (isTokenRequired) {
        options = {
          headers: {
            "Content-Type": "application/json",
            Authorization: `bearer ${localStorage.getItem("Token")}`,
          },
        };
      }
      const method = !_.isEmpty(queryParam) && queryParam.id ? "put" : "post";
      return Axios[method](
        `${process.env.VUE_APP_API_URL}${url}`,
        data,
        options
      );
    },

    getApi(endPoint, queryParam = {}, isTokenRequired = true) {
      let url;
      let options = {
        headers: {
          "Content-Type": "application/json",
        },
      };
      if (isTokenRequired) {
        options = {
          headers: {
            "Content-Type": "application/json",
            Authorization: `bearer ${localStorage.getItem("Token")}`,
          },
        };
      }
      if (!_.isEmpty(queryParam)) {
        const queryParamString = this.getQueryStringFromObject(queryParam);
        url = `${endPoint}?${queryParamString}`;
      } else {
        url = endPoint;
      }
      return Axios.get(`${process.env.VUE_APP_API_URL}${url}`, options);
    },

    deleteApi(endPoint, queryParam = {}, isTokenRequired = true) {
      let url;
      let options = {
        headers: {
          "Content-Type": "application/json",
        },
      };
      if (isTokenRequired) {
        options = {
          headers: {
            "Content-Type": "application/json",
            Authorization: `bearer ${localStorage.getItem("Token")}`,
          },
        };
      }
      if (!_.isEmpty(queryParam)) {
        const queryParamString = this.getQueryStringFromObject(queryParam);
        url = `${endPoint}?${queryParamString}`;
      } else {
        url = endPoint;
      }
      return Axios.delete(`${process.env.VUE_APP_API_URL}${url}`, options);
    },
  },
};
export default axiosMixin;
