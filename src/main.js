import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

import API from "./api";
import App from "./App.vue";
import BookComponent from "./components/book.vue";
import BootstrapVue from "bootstrap-vue";
import DashboardComponent from "./components/dashboard.vue";
import DeviceComponent from "./components/devices.vue";
import LoginComponent from "./components/loginform.vue";
import Toasted from "vue-toasted";
import Vue from "vue";
import VueRouter from "vue-router";
import Vuex from "vuex";
import _ from "underscore";
import Axios from "axios";

/**
 * ! Third Party Utilities  */
Vue.use(BootstrapVue);
Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(_);
Vue.use(Toasted, {
  duration: 3000,
  position: "bottom-left",
});

Object.defineProperty(Vue.prototype, "$api", { value: API });
Vue.config.productionTip = false;

/**
 * ! Routes Define  */
const routes = [
  { path: "/", redirect: "/home" },
  { path: "/login", name: "Login", component: LoginComponent },
  {
    path: "/home",
    name: "dashboard-home",
    component: DashboardComponent,
    children: [
      { path: "book", name: "book-list", component: BookComponent },
      { path: "devices", name: "device-list", component: DeviceComponent },
    ],
  },
];
const router = new VueRouter({
  mode: "history", // Using history mode to remove hash from browser Url
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.path !== "/login") {
    if (!_.isEmpty(localStorage.getItem("Token"))) {
      Axios.get(process.env.VUE_APP_API_URL, {
        headers: {
          "Content-Type": "application/json",
          Authorization: `bearer ${localStorage.getItem("Token")}`,
        },
      })
        .then((res) => {
          console.log(res);
          next();
        })
        .catch((err) => {
          console.log(err);
          next("/login");
        });
    } else {
      next("/login");
    }
  } else {
    next();
  }
});
new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
